package frc.team8060;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

public class Dashboard {

    // TODO : finish this shit

    private static NetworkTable table;

    static {
        setNetworkTableInstance(NetworkTableInstance.getDefault());
    }

    public static synchronized void setNetworkTableInstance(NetworkTableInstance inst) {
        Dashboard.table = inst.getTable("Dashboard");
    }

    public static NetworkTableEntry getEntry(String key) {
        return table.getEntry(key);
    }

    /**
     * Adds a Subsystem status to be displayed on the dashboard
     *
     * @param name the name of the Subsystem
     * @param status if the Subsystem was initialized successfully
     */
    public static void addSubsystemStatus(String name, boolean status) {
        getEntry("sub_sys" + name).setBoolean(status);
    }

}
