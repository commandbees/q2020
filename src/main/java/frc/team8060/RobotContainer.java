package frc.team8060;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.team8060.commands.*;
import frc.team8060.constants.ArmConstants;
import frc.team8060.subsystems.*;
import frc.team8060.util.RamseteCommandHelper;

import static frc.team8060.constants.DriveConstants.JOYSTICK_PORT;
import static frc.team8060.constants.DriveConstants.XBOXCONTROLLER_PORT;


/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
    private final Joystick joystick = new Joystick(JOYSTICK_PORT);
    // private final XboxController xboxController = new XboxController(XBOXCONTROLLER_PORT);

    // subsystems
    private Drivetrain drivetrain;
    private PiVision piVision;
    private ArmControl armControl;
    private IntakeControl intakeControl;

    public RobotContainer() {
        subsystemInit();
        if (drivetrain != null)
            drivetrain.setDefaultCommand(new TeleopControl(drivetrain, joystick));
        configureButtonBindings();
    }

    public Command getAutonomousCommand() {
        // return null;
        return new AutoCommand(drivetrain);
    }

    private void configureButtonBindings() {
        if (armControl != null) {

            // reset arm encoder
            // DO NOT PRESS UNLESS ARM IS STRAIGHT UP
            /*
            new Trigger(() -> xboxController.getRawButton(ArmConstants.ARM_ENCODER_RESET_BUTTON)).onTrue(
                    new ArmEncoderResetCommand(armControl)
            );
             */
            new Trigger(() -> joystick.getRawButton(ArmConstants.INTAKE_IN_BUTTON)).whileTrue(
                    new IntakeControlCommand(
                            intakeControl,
                            ArmConstants.INTAKE_REVERSED
                    )
            );
            new Trigger(() -> joystick.getRawButton(ArmConstants.INTAKE_OUT_BUTTON)).whileTrue(
                    new IntakeControlCommand(
                            intakeControl,
                            !ArmConstants.INTAKE_REVERSED
                    )
            );
            new Trigger(() -> joystick.getRawButton(ArmConstants.ARM_RAISE_BUTTON)).whileTrue(
                    new ArmControlCommand(
                            armControl,
                            ArmConstants.ARM_REVERSED
                    )
            );
            new Trigger(() -> joystick.getRawButton(ArmConstants.ARM_LOWER_BUTTON)).whileTrue(
                    new ArmControlCommand(
                            armControl,
                            !ArmConstants.ARM_REVERSED
                    )
            );


            /*
            button(ArmConstants.ARM_LOWER_BUTTON).toggleOnTrue(new ArmControlCommand(
                    armControl,
                    !ArmConstants.ARM_REVERSED
            ));

             */
        }
        /*
        button(ArmConstants.INTAKE_IN_BUTTON).whileTrue(new IntakeControlCommand(
                intakeControl,
                joystick,
                ArmConstants.INTAKE_REVERSED
        ));
        button(ArmConstants.INTAKE_OUT_BUTTON).whileTrue(new IntakeControlCommand(
                intakeControl,
                joystick,
                !ArmConstants.INTAKE_REVERSED
        ));
         */
    }
    // initializes all the subsystems, checking which ones are working
    // I've put this all down here to not clutter up the import bits of RobotContainer
    private void subsystemInit() {

        // shitty repeated code rip
        try {
            drivetrain = new Drivetrain();
            Dashboard.addSubsystemStatus("drivetrain", true);
        } catch (Exception e) {
            e.printStackTrace();
            Dashboard.addSubsystemStatus("drivetrain", false);
        }
        try {
             piVision = new PiVision();
             Dashboard.addSubsystemStatus("piVision", true);
        } catch (Exception e) {
            e.printStackTrace();
            Dashboard.addSubsystemStatus("piVision", false);
        }
        try {
            armControl = new ArmControl();
            Dashboard.addSubsystemStatus("armControl", true);
        } catch (Exception e) {
            e.printStackTrace();
            Dashboard.addSubsystemStatus("armControl", false);
        }
        try {
            intakeControl = new IntakeControl();
            Dashboard.addSubsystemStatus("intakeControl", true);
        } catch (Exception e) {
            e.printStackTrace();
            Dashboard.addSubsystemStatus("intakeControl", false);
        }

    }

}
