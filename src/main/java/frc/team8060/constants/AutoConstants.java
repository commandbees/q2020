package frc.team8060.constants;

public class AutoConstants {
    // TODO : tune constants to needed values

    public static final double MAX_SPEED_METERS_PER_SECOND = 3;
    // max speed meters per second
    public static final double MAX_ACCELERATION_METERS_PER_SECOND_SQUARED = 1;
    // max acceleration meters per second squared

    // allegedly "reasonable" baseline values (2, 0.7)
    // can be fined tuned later on if needed
    public static final double RAMSETE_B = 2;
    public static final double RAMSETE_ZETA = 0.7;

}
