package frc.team8060.subsystems

import edu.wpi.first.networktables.NetworkTableInstance
import edu.wpi.first.networktables.TimestampedDoubleArray
import edu.wpi.first.wpilibj2.command.SubsystemBase

class PiVision: SubsystemBase() {

    /*
        double x
        double y
        double z
        double arctan
        double qx
        double qy
        double qz
        double qw
     */

    // [ 0.1981076, 0.1387165, 0.1981076, 0.949876 ]
    // [ -0.1387165, 0.1981076, -0.1387165, -0.9603483 ]

    data class Quaternion(val x: Double, val y: Double, val z: Double, val w: Double)

    fun Quaternion.invert(): Quaternion {
        val norm = x * x + y * y + z * z + w * w
        return Quaternion(-x / norm, -y / norm, -z / norm, w / norm)
    }

    private val table = NetworkTableInstance.getDefault().getTable("visionTable")

    private val subscribers = Array(8) {
            i -> table.getDoubleArrayTopic("tag$i").subscribe(doubleArrayOf())
    }

    private var lastChange: Long = 0

    fun getTagData(i: Int): TimestampedDoubleArray {
        return subscribers[i].atomic
    }

    override fun periodic() {

        var mostRecent = Pair<Int, Long>(-1, -1)

        subscribers.forEachIndexed() { index, doubleArraySub ->
            // check to see if anything has updated
            // if something has changed call Drivetrain.resetOdometry(<pose from tag>)

            if (doubleArraySub.lastChange < mostRecent.second) {
                mostRecent = Pair(index, doubleArraySub.lastChange)
            }
        }

    }

}
