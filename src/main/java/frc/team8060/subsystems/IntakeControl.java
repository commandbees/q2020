package frc.team8060.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.motorcontrol.PWMMotorController;
import edu.wpi.first.wpilibj.motorcontrol.PWMSparkMax;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.team8060.constants.ArmConstants;
import frc.team8060.constants.DriveConstants;

public class IntakeControl extends SubsystemBase {


    CANSparkMax motorController = new CANSparkMax(ArmConstants.INTAKE_MOTOR_PORT, CANSparkMaxLowLevel.MotorType.kBrushed);

    public IntakeControl() {
    }

    public void runMotor(double voltage) {
        motorController.set(voltage);
    }

    public void stopMotor() {
        motorController.set(0);
        motorController.stopMotor();
    }
}
