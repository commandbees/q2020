package frc.team8060.util;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.RamseteController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import frc.team8060.constants.AutoConstants;
import frc.team8060.constants.DriveConstants;
import frc.team8060.subsystems.Drivetrain;

import java.util.List;

public class RamseteCommandHelper {

    public static Command createCommand(Drivetrain drivetrain, List<Pose2d> points){
        TrajectoryConfig config = generateTrajectoryConfig();
        Trajectory trajectory = TrajectoryGenerator.generateTrajectory(
                points,
                config
        );
        return generateCommand(drivetrain, trajectory);
    }

    public static Command generateGotoCommand(Drivetrain drivetrain, Pose2d end) {
        TrajectoryConfig config = generateTrajectoryConfig();
        Trajectory trajectory = TrajectoryGenerator.generateTrajectory(
                List.of(point(0, 0, 0), end),
                config
        );
        return generateCommand(drivetrain, trajectory);
    }

    /**
     * simple test ramsete command that moves +1 in both x and y directions
     * zeroes odometry
     * @param drivetrain
     * @return the command
     */
    public static Command generateTestCommand(Drivetrain drivetrain) {
        TrajectoryConfig config = generateTrajectoryConfig();
        drivetrain.zeroOdometry();
        Trajectory trajectory = TrajectoryGenerator.generateTrajectory(
                List.of(point(0.0, 0.0, 0.0), point(1.0, 1.0, 0.0)),
                config
        );
        return generateCommand(drivetrain, trajectory);
    }

    private static Command generateCommand(Drivetrain drivetrain, Trajectory trajectory) {
        RamseteCommand ramseteCommand = new RamseteCommand(
                trajectory,
                drivetrain::getPose,
                new RamseteController(
                        AutoConstants.RAMSETE_B, AutoConstants.RAMSETE_ZETA
                ),
                new SimpleMotorFeedforward(
                        DriveConstants.S_VOLTS,
                        DriveConstants.V_VOLT_SECONDS_PER_METER,
                        DriveConstants.A_VOLT_SECONDS_SQUARED_PER_METER
                ),
                DriveConstants.DRIVE_KINEMATICS,
                drivetrain::getWheelSpeeds,
                new PIDController(DriveConstants.PDRIVE_VEL, 0.0, 0.0),
                new PIDController(DriveConstants.PDRIVE_VEL, 0.0, 0.0),
                drivetrain::tankDriveVolts,
                drivetrain
        );
        return ramseteCommand;
    }

    private static TrajectoryConfig generateTrajectoryConfig(){
        DifferentialDriveVoltageConstraint autoVoltageConstraint = new DifferentialDriveVoltageConstraint(
                new SimpleMotorFeedforward(
                        DriveConstants.S_VOLTS,
                        DriveConstants.V_VOLT_SECONDS_PER_METER,
                        DriveConstants.A_VOLT_SECONDS_SQUARED_PER_METER
                ),
                DriveConstants.DRIVE_KINEMATICS,
                10.0
        );

        TrajectoryConfig config = new TrajectoryConfig(
                AutoConstants.MAX_SPEED_METERS_PER_SECOND,
                AutoConstants.MAX_ACCELERATION_METERS_PER_SECOND_SQUARED
        );
        config.setKinematics(DriveConstants.DRIVE_KINEMATICS);
        config.addConstraint(autoVoltageConstraint);
        return config;
    }

    // shorter helper function to make Pose2d object with less boilerplate
    private static Pose2d point(double x, double y, double rot) {
        return new Pose2d(x, y, new Rotation2d(rot));
    }
}
