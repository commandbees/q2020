package frc.team8060.util;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * An implementation of a Lazy loaded value.
 */
public final class Lazy<T> {

    private Supplier<T> builder;
    private volatile T value;

	public Lazy(Supplier<T> builder) {
        this.builder = builder;
    }

    public T get() {
        final T result = value;
        return result == null ? maybeCompute() : result;
    }

    private synchronized T maybeCompute() {
        if (value == null) {
            value = Objects.requireNonNull(this.builder.get());
        }
        return value;
    }
}
