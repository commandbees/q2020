package frc.team8060.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.team8060.subsystems.IntakeControl;

public class IntakeControlCommand extends CommandBase {

    private final IntakeControl intakeControl;

    private double power = 1;

    public IntakeControlCommand(IntakeControl intakeControl, boolean reversed) {
        this.intakeControl = intakeControl;
        if (reversed) {
            power *= -1;
        }
        addRequirements(intakeControl);
    }

    @Override
    public void execute() {
        intakeControl.runMotor(power);
    }

    @Override
    public void end(boolean interrupted) {
        intakeControl.stopMotor();
    }
}