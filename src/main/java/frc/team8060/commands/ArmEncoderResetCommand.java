package frc.team8060.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.team8060.subsystems.ArmControl;

public class ArmEncoderResetCommand extends CommandBase {

    private final ArmControl armControl;

    public ArmEncoderResetCommand(ArmControl armControl) {
        this.armControl = armControl;
    }

    @Override
    public void execute() {
        armControl.manualEncoderReset();
    }
}
